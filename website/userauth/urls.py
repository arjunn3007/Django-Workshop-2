from django.conf.urls import url,include
from . import views
from django.contrib.auth import views as authViews
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
urlpatterns=[
	url(r'^signup/$',views.signup,name='signup'),
	url(r'^login/$',views.login_user,name='login.html'),
	url(r'^logout/$',authViews.logout,{'template_name':'userauth/logout.html'}),
	url(r'^home/',TemplateView.as_view(template_name='userauth/home.html'))
]

urlpatterns+=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
